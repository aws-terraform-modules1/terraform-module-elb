variable "ALB" {
  type = bool
  description = "(Boolean, Optional) specifies if you want this resource to be created or not"
}

variable "ALB_Name" {
  type        = string
  description = "(string, Required) Whatever you want to call your AWS Application LoadBalancer, only alphanumeric"
  default     = "balancerofloadsApplication"
}

variable "ALB_Subnets" {
  type        = set(string)
  description = "(set(string), Optional) The ID of the subnet your instances will be in. (aws_subnet.example.id)"
}

variable "ALB_Env" {
  type        = string
  description = "(string, Optional) Either Production or Development, whatever stage you're in."
}

variable "ALB_Bucket_Name" {
  type        = string
  description = "(string, Optional) if you want to store logs in a bucket for Application LB, this would be its name."
}

variable "ALB_S3_Logs" {
  type        = bool
  description = "(Boolean, Optional) default is false even if you specify a name for the bucket."
  default     = false
}

variable "ALB_Bucket" {
  type = bool
  description = "(Boolean, Optional) specifies if you want this resource to be created or not"
}

variable "NLB" {
  type = bool
  description = "(Boolean, Optional) specifies if you want this resource to be created or not"
}

variable "NLB_Name" {
  type        = string
  description = "(string, Required) Whatever you want to call your AWS Network LoadBalancer, only alphanumeric"
  default     = "balancerofloadsNetwork"
}

variable "NLB_Subnets" {
  type        = set(string)
  description = "(set(string), Optional) The ID of the subnet your instances will be in. (aws_subnet.example.id)"
}

variable "NLB_Env" {
  type        = string
  description = "(string, Optional) Either Production or Development, whatever stage you're in."
}

variable "NLB_Bucket_Name" {
  type        = string
  description = "(string, Optional) if you want to store logs in a bucket for Application LB, this would be its name."
}

variable "NLB_S3_Logs" {
  type        = bool
  description = "(Boolean, Optional) default is false even if you specify a name for the bucket."
  default     = false
}

variable "NLB_Bucket" {
  type = bool
  description = "(Boolean, Optional) specifies if you want this resource to be created or not"
}