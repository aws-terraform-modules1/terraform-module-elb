resource "aws_lb" "odai_ALB" {
  #count                      = var.ALB ? 1 : 0
  name                       = var.ALB_Name
  load_balancer_type         = "application"
  enable_deletion_protection = false
  subnets                    = var.ALB_Subnets
  tags = {
    Environment = var.ALB_Env
  }
  access_logs {
    bucket  = aws_s3_bucket.ALB_Logger.id
    prefix  = "ALB-BINGUS-"
    enabled = var.ALB_S3_Logs
  }
}

resource "aws_lb" "odai_NLB" {
  #count                      = var.NLB ? 1 : 0
  name                       = var.NLB_Name
  load_balancer_type         = "network"
  enable_deletion_protection = false

  subnets = var.NLB_Subnets

  tags = {
    Environment = var.NLB_Env
  }
  access_logs {
    bucket  = aws_s3_bucket.NLB_Logger.id
    prefix  = "NLB-BINGUS-"
    enabled = var.NLB_S3_Logs
  }
}

resource "aws_s3_bucket" "ALB_Logger" {
  #count  = var.ALB_Bucket ? 1 : 0
  bucket = var.ALB_Bucket_Name

  tags = {
    Name        = var.ALB_Bucket_Name
    Environment = var.ALB_Env
  }
}

resource "aws_s3_bucket" "NLB_Logger" {
  #count  = var.NLB_Bucket ? 1 : 0
  bucket = var.NLB_Bucket_Name

  tags = {
    Name        = var.NLB_Bucket_Name
    Environment = var.NLB_Env
  }
}